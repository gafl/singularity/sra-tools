#!/bin/bash

#INRA GAFL 2019
#jacques Lagnel
#MIT License

#NCBI sra toolkit
# download and convert SRR in fastq gzipped files.

#example: SRR7279491 Paired-end
#gives 2 fastq files:
#SRR7279491_1.fastq.gz
#SRR7279491_2.fastq.gz

listsrr=$1
#--------- check if we have the SRR file -----------------------------
if [ -z "$listsrr" ] || [ ! -f "$listsrr" ]
then
	echo "retrieve SRR and convert in fastq.gz (split paired-end)"
	echo "usage:"
        echo "$0 <list of SRR 1by line>"
        exit 0
fi

echo "SRR list: $listsrr"

#-------- for each SRR do: download and convert SRR in fastq.gz -------
for srr in $(cat $listsrr)
do
   echo $srr
   # remove the & in the command for serial download
   #sra tool kit in path:
   #fastq-dump --gzip --split-files $srr &
   #container singularity:
   singularity exec sra-tools_v2.10.7.sif fastq-dump --gzip --split-files $srr &
done

exit 0


