# sra-tools Singularity container
### Bionformatics package sra-tools<br>
SRA Toolkit and SDK from NCBI<br>
sra-tools Version: 2.10.7<br>
[https://github.com/ncbi/sra-tools]

Singularity container based on the recipe: Singularity.sra-tools_v2.10.7

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build sra-tools_v2.10.7.sif Singularity.sra-tools_v2.10.7`

### Get image help
`singularity run-help ./sra-tools_v2.10.7.sif`

#### Default runscript: STAR
#### Usage:
  `sra-tools_v2.10.7.sif --help`<br>
    or:<br>
  `singularity exec sra-tools_v2.10.7.sif fastq-dump --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull sra-tools_v2.10.7.sif oras://registry.forgemia.inra.fr/gafl/singularity/sra-tools/sra-tools:latest`


